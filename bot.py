#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from jsonrpcclient.exceptions import JsonRpcClientError
from jsonrpcclient.http_client import HTTPClient
from jsonrpcclient.request import Request
from requests.exceptions import RequestException
from telegram.ext import (
    CommandHandler,
    Filters,
    Updater,
)

TOKEN = '418351214:AAHi5-YzTuQO9nclWx-1E6PWHHCiJkk5kdw'
MY_ID = 406912440

RIG_ENDPOINT = 'http://10.8.0.10:5000'

MINERS = {
    'claymore': {'min_hashrate': 70},
    'ethminer': {'min_hashrate': 70},
    'zm': {'min_hashrate': 950},
    'bminer': {'min_hashrate': 1000},
    'ccminer': {'min_hashrate': 2250},
    'xmrig': {'min_hashrate': 1600},
}

TIMEOUT = 3


# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG
)

log = logging.getLogger(__name__)


def rpc(url, method):
    client = HTTPClient(url)
    request = Request(method)
    try:
        return client.send(request, timeout=TIMEOUT)
    except (RequestException, JsonRpcClientError) as e:
        log.error('Ferm HTTP error: {}'.format(e))
        return None


def start_handler(bot, update):
    help_msg = '\n'.join([
        '/miner - Current miner info.',
        '/gpu - GPU information.',
        '/help - Shows this message.',
    ])

    update.message.reply_text(help_msg)


def miner_handler(bot, update):

    data = rpc(RIG_ENDPOINT, 'miner')

    if data is None:
        update.message.reply_text('I am having problems contacting with the rig.')
        return

    alive_miners = filter(None, data.values())

    if not alive_miners:
        update.message.reply_text('No alive miners on the rig.')
    else:
        for m in alive_miners:
            update.message.reply_text(m, parse_mode='markdown')


def gpu_handler(bot, update):

    data = rpc(RIG_ENDPOINT, 'gpu')

    if data is None:
        update.message.reply_text('I am having problems contacting with the rig.')
        return

    update.message.reply_text(data, parse_mode='markdown')


def watchdog(bot, job):

    data = rpc(RIG_ENDPOINT, 'hashrate')

    if data is None:
        bot.send_message(job.context, 'I am having problems contacting with the rig.')
        return

    alive_miners = {name: hr for name, hr in data.items() if hr is not None} if data else None

    if not alive_miners:
        bot.send_message(job.context, text='No alive miners on the rig.')
        return

    for name, hr in alive_miners.items():
        if hr < MINERS[name]['min_hashrate']:
            msg = 'Low hashrate *{}* on miner *{}*.'.format(hr, name)
            bot.send_message(job.context, msg, parse_mode='markdown')


def error_handler(bot, update, error):
    log.warning('Update "%s" caused error "%s"', update, error)


if __name__ == '__main__':
    updater = Updater(TOKEN)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler(
        ['start', 'help'],
        start_handler,
        filters=Filters.user(user_id=MY_ID)
    ))
    dp.add_handler(CommandHandler(
        'miner',
        miner_handler,
        filters=Filters.user(user_id=MY_ID)
    ))
    dp.add_handler(CommandHandler(
        'gpu',
        gpu_handler,
        filters=Filters.user(user_id=MY_ID)
    ))

    dp.add_error_handler(error_handler)

    dp.job_queue.run_repeating(watchdog, 60, context=MY_ID)

    updater.start_polling()
    updater.idle()
